\ matrix defining word
\ definition: x-size y-size matrix winmatrix
\ runtime x-pos y-pos winmatrix ( addr )
\ calculate address by multiplying x-pos by y-size and adding y-pos
\ structure: y-size x-size data...
: matrix ( x-size y-size "name )
  create 2dup , , * cells allot
does> ( x y -- addr )
  ( x-pos y-pos addr ) dup @ 
  ( x-pos y-pos addr y-size ) 2swap 
  ( addr y-size x-pos y-pos ) -rot
  ( addr y-pos y-size x-pos ) * + cells +
  ( cell-addr ) 2 cells + ;

: clean ( 0-0-matrix-addr -- )
  dup 2 cells - @ ( addr x-size )
  over cell - @   ( addr x-size y-size )
  * cells erase ;

: .matrix ( 0-0-matrix-addr -- )
  dup 2 cells - @ ( addr x-size )
  over cell - @   ( addr x-size y-size )
  2dup swap cr . ." by " .
  0 do cr ( y-rows ) 
    dup 0 do  ( x-cols ) 
      ( addr x-size ) swap 
      ( x-size addr ) dup dup
      ( x-size addr addr addr ) cell - @
      ( x-size addr addr y-size ) i * j + cells
      ( x-size addr addr offset ) + @ . ."  "
      ( x-size addr ) swap
    loop
  loop drop drop ;
      
  
