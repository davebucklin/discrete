\ My attempt at N Queens for the Mathematical Thinking in Computer
\ Science course. Brute-force approach.

8 constant size                     \ size of the chess board
create permutation size cells allot
variable offset                     \ pointer into the permutation array
variable count                      \ number of solutions found

: init ( -- )
  permutation size cells $ff fill
  1 offset ! 
  0 count ! ;

: ok? ( f f -- f )
  or invert ;

: value ( -- addr )
  permutation offset @ cells + ;

: increment ( addr -- )
  1 swap +! ;

: decrement ( addr -- )
  dup @ 1- swap ! ;

: toobig? ( -- f )
  value @ size = ;

: used? ( -- f )
  0                     \ flag
  offset @ 0 ?do      \ from 0 to offset
    permutation i cells + @ \ get the value to compare
    value @                 \ get the current value
    = if drop -1 leave then
  loop ;

: done? ( -- f )
  offset @ -1 = ;

: perm ( -- )
  value increment
  used? toobig? dup if -1 value ! offset decrement then
  done? if 2drop exit then
  ok?   if offset increment then ;

: permute ( -- )
  offset decrement
  begin perm offset @ size = done? or until ;

: column ( p -- j )
  noop ;

: row ( p -- i )
  cells permutation + @ ;

\ abs (i(1) - i(2)) = abs (j(1) - j(2)
: diag? ( point1 point2 -- f )
  2dup ( p1 p2 p1 p2 )
  column swap column - abs -rot
  row swap row - abs = ;

\ look for queens that share a diagonal
: check ( -- f )
  -1         \ flag
  size 1 - 0 do
    size i 1+ do
      j i diag? if drop 0 leave then
    loop
    dup invert if leave then
  loop ;

\ output the current permutation
: found ( -- )
  cr
  size 0 do
    i cells permutation + @ $30 + emit
  loop ;

: nqueens ( -- )
  utime drop
  init
  begin
    permute
  done? invert while
    check if found count increment then
  repeat 
  utime drop swap - 
  cr cr ." Found " 
  count @ .
  ."  solutions in " #1000 / . ."  ms" ;

