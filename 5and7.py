# This function was written as a recursion exercise for the Mathematical
# Thinking in Computer Science course from UC San Diego.

def change(amount):
  if amount == 24:
    return [5, 5, 7, 7]
  if amount == 25:
    return [5, 5, 5, 5, 5]
  if amount == 26:
    return [5, 5, 5, 7, 7, 7]
  if amount == 27:
    return [5, 5, 5, 5, 7]
  if amount == 28:
    return [7, 7, 7, 7]
  if amount == 29:
    return [5, 5, 5, 7, 7]
  if amount == 30:
    return [5, 5, 5, 5, 5, 5]
  x = change(amount - 7)
  x.append(7)
  return x

