\ Brute-force solution for More Conditional Probabilities quiz
\ in the Combinatorics and Probabilities course.
\ Given values are P(B) = .5, P(B|A) = .8, P(B|Ac) = .4
\ need to solve for P(A)
\ I didn't see an algebraic solution in the coursework.
\ This started as an attempt to sanity-check the problem as given.
\ I assumed S = 1000 and scaled up numerators by 1000 for division operations.
\ The result is therefore scaled up by a factor of 1000.

variable a
500 constant b
1000 constant s

\ Intersection of A and B
: AandB ( -- n )
  a @ 5 / 4 * ;

: B-AandB ( -- n )
  b AandB - ;

: !A ( -- n )
  s a @ - ;

: PBgiven!A ( -- m q )
  B-AandB 1000 * !A /mod ;

: show ( -- )
  cr ." a=" a @ .
  cr ." !a=" !A .
  cr ." B^A=" AandB .
  cr ." B|!A=" PBgiven!A . . ;

: main ( -- )
  0 a !
  begin
  5 a +!
  a @ s < while
  PBgiven!A 400 = if 0= if show then then
  repeat ;
