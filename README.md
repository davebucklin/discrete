# Forth Exercises

For the Introduction to Discrete Mathematics for Computer Science specialization
offered by UC San Diego through Coursera.

The specialization comprises five courses:

1. Mathematical Thinking in Computer Science
2. Combinatorics and Probability
3. Introduction to Graph Theory
4. Number Theory and Cryptography
5. Delivery Problem

All are written for Gforth 0.7.3.
