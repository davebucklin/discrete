\ Branch and bound TSP solver attempt
decimal

\ Graph

variable #vertices
create vertices
here
2 , 2 , 7 , 1 , 11 , 2 ,
13 , 6 , 3 , 6 , 4 , 9 ,
18 , 8 , 2 , 12 , 14 , 12 ,
10 , 16 ,
\ 3 , 16 , 6 , 19 , 12 , 20 ,
here swap - cell / 2 / #vertices !

\ create adjacency matrix
create matrix #vertices @ dup * floats allot
: matclear ( -- )
  #vertices @ dup * 0 ?do 0 s>f matrix i floats + f! loop ;

: v>x ( vertex -- x )
  2 * cells vertices + @ ;

: v>y ( vertex -- y )
  2 * 1+ cells vertices + @ ;

: v>xy ( vertex -- x y )
  dup v>x swap v>y ;

: dist ( v-from v-to -- f:dist )
  v>xy rot v>xy
  rot swap - dup * s>f
  swap - dup * s>f
  f+ fsqrt ;

: offset ( mat-x mat-y -- offset )
  #vertices @ * + ;

\ calculate edge weights and store in adjacency matrix
\ future idea: optimize matrix to eliminate duplicate data, self loops.
\ i.e., for a 6-vertex graph, we only need 15 weight values.
: matbuild ( -- )
  matclear
  #vertices @ 0 ?do
    #vertices @ i 1+ ?do
      j i dist fdup
      matrix j i offset floats + f!
      matrix i j offset floats + f!
    loop
  loop ;

: weight ( v-from v-to -- f:weight)
  offset floats matrix + f@ ;

: ++ ( addr -- )
  1 swap +! ;

: -- ( addr -- )
  dup @ 1- swap ! ;

\ Path Tree

\ Building this tree uses up tons of dictionary space.
\ and will overflow the dictionary if the graph has much more than ten vertices.
\ Possible improvement could be to use heap memory (allocate) and some garbage collection.

variable root-node
variable current
fvariable path-weight
fvariable best-weight
create visited #vertices @ cells allot
create best-path #vertices @ cells allot
variable level
variable done 0 done !

: clear
  visited #vertices @ cells erase
  best-path #vertices @ cells erase
  1 s>f 0 s>f f/ ( infinity ) best-weight f!
  0 s>f path-weight f!
  root-node @ current !
  0 level ! 
  0 done ! ;

: .path
  #vertices @ 0 do best-path i cells + @ . loop ;

\ node structure: ID | parent | child | sibling

\ Create a new node
: node ( id -- addr )
  here swap , 0 , 0 , 0 , ;

: parent ( addr -- addr )
  cell + ;

: child ( addr -- addr )
  2 cells + ;

: sibling ( addr -- addr )
  3 cells + ;

: lastchild ( addr -- addr )
  child @ begin dup sibling @ dup 0 <> while nip repeat drop ;

: visit ( id -- )
  cells visited + 1 swap ! ;

: unvisit ( id -- )
  cells visited + 0 swap ! ;

\ add child to current node
: add ( addr -- )
  dup current @ child @ 0 <> if \ find last child and add sibling
    current @ lastchild sibling !
  else
    current @ child ! \ add child
  then
  parent current @ swap ! ; \ set parent

\ create node for source vertex and
\ set current node pointer
0 node dup root-node ! current !

\ find the next unvisited node
: forward ( -- node-id | 0 )
  0 begin
    1+ dup #vertices @ <= if
      dup cells visited + @
    else drop 0 0 then
  0 <> while repeat ;

: path+ ( v-from v-to -- )
  weight path-weight f@ f+ path-weight f! ;

: path- ( v-from v-to -- )
  weight path-weight f@ fswap f- path-weight f! ;

: >parent ( -- )
  current @ @ unvisit
  current @ parent @ current ! ;

\ build the list of visited nodes
: ?visited ( -- )
  visited #vertices @ cells erase
  current @ begin dup @ visit parent @ dup 0 <> while repeat drop \ current and all parents going up
  current @ child @ begin dup 0 <> while dup @ visit sibling @ repeat drop ; \ all direct children

: .visited ( -- )
  #vertices @ 0 ?do visited i cells + @ 0 <> if i . then loop ;

: .node ( addr -- )
  cr dup ." addr=" hex. cr
  ." ID=" dup @ . cr
  dup child @ ." Child=" hex. cr
  dup sibling @ ." Sibling=" hex. cr
  parent @ ." Parent=" hex. cr ;

: lead-in ( -- )
  level @ 0 ?do [char] - emit loop ;

: .tree ( nodeaddr -- )
  cr lead-in dup @ .
  dup child @ dup
  0 <> if level ++ recurse level -- else drop then
  sibling @ dup 0 <> if recurse else drop then ;

\ follow parent links to build a path
: >path ( -- )
  #vertices @ 1- current @
  begin
    dup @ rot tuck best-path swap cells + ! ( addr vidx )
    1- swap parent @
  dup 0= until 2drop ;

: root? ( -- f )
  current @ root-node @ = ;

: backup ( -- )
  current @ dup @ swap parent @ @ path- >parent ;

: bnb ( -- )
  clear
  matbuild
  begin
    ?visited                                    \ room for improvement here. We rebuild the list of visited nodes every iteration.
    root? if 0 s>f path-weight f! then          \ if we're at the root node, zero out the path weight
    forward                                     \ get next node
    dup 0= if                                   \ all nodes visited?
      drop
      current @ child @ 0= if                   \ leaf node
        current @ @ root-node @ @ 2dup path+    \ add weight back to source
        path-weight f@ best-weight f@ f< if     \ path weight lower than current best
          path-weight f@ best-weight f! >path   \ store current path and path weight
        then
        path-                                   \ subtract weight to source
      then
      root? if -1 done ! else                   \ if current node = root node, we're done
        backup                                  \ reduce path weight and go to parent
      then
    else
      dup visit node dup add dup current !      \ create child for next non-visited node and set to current
      dup parent @ @ swap @ path+               \ retrieve weight and add to path weight
      path-weight f@ best-weight f@ f> if       \ weight > best weight
        backup                                  \ subtract weight and go to parent
      then
    then
  done @ until 
  cr ." best weight=" best-weight f@ f.
  cr ." best path=" .path ;

: treetest ( -- )
  0 level !
  root-node @ current !
  1 node dup add
  2 node add
  current !
  3 node dup add
  current !
  4 node add
  root-node @ .tree ;

