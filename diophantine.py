def gcd(a, b):
  assert a >= b and b >= 0 and a + b > 0

  if b == 0:
    d, x, y = a, 1, 0
  else:
    (d, p, q) = gcd(b, a % b)
    x = q
    y = p - q * (a // b)

  assert a % d == 0 and b % d == 0
  assert d == a * x + b * y
  return (d, x, y)

def diophantine(a, b, c):
  if ( a >= b ) :
      (d, x, y) = gcd(a, b)
  else:
      (d, y, x) = gcd(b, a)

  assert c % d == 0
  # return (x, y) such that a * x + b * y = c
  mult = c / d
  x = x * mult
  y = y * mult
  return (x, y)

print( 'dio(10,6,14)')
print( diophantine(10,6,14))

print( 'dio(3,5,22)')
print( diophantine(3,5,22))

print( 'dio(17,11,1)')
print( diophantine(17,11,1))
