\ Greatest common divisor

: gcd-assert ( a b -- a b )
  assert( 2dup >= ) 
  assert( dup 0 >= )
  assert( 2dup + 0 > ) ;

: gcd ( a b -- d )
  gcd-assert
  dup 0 > if tuck mod recurse else drop then ;

\ Find Bezout coefficients
: bezout ( a b d p q -- d x y )
  rot >r ( a b p q ) ( r: d )
  2swap / ( p q a/b )
  over * ( p q q*a/b )
  rot swap - ( q p-q*a/b )
  r> -rot ( d x y ) ;

\ extended GCD
: egcd ( a b -- d x y )
  gcd-assert
  dup 0= if
    drop 1 0
  else
    2dup tuck mod recurse ( a b d p q )
    bezout ( d x y )
  then ;

\ Least common multiple
: lcm ( a b -- n )
  2dup < if swap then 2dup * -rot gcd / ;

\ Diophantine solver

\ solution exists iff gcd(a,b) | c
: possible ( a b c -- f )
  -rot 2dup ( c a b a b )
  < if swap then gcd ( c d ) mod 0= ;

\ get certificate for gcd(a,b)
: dio-dxy ( a b -- d x y )
  2dup >= if egcd else swap egcd swap then ;

: 3dup ( a b c -- a b c a b c )
  2 pick 2 pick 2 pick ;

\ given a, b, and c for ax + by = c, find x and y
: dio ( a b c -- x y )
  3dup assert( possible ) 
  -rot dio-dxy ( c d x y )
  \ here we need to multiply x and y by the quotient of c/d
  2swap / tuck ( x c/d y c/d )
  * -rot * swap ( x y ) ;

: dio-test ( a b c -- )
  3dup dio ( a b c x y )
  2dup swap ." x=" . ." y=" .
  swap >r >r -rot ( c a b )
  r> * ( c a by )
  swap r> * ( c by ax )
  + = if ." LGTM" else ." :(" then ;

\ Modular division

\ n > 1, a > 0, gcd(a,n) == 1
: divide-assert ( a n -- )
  assert( dup 1 > )
  assert( over 0 > )
  assert( 2dup < if swap then gcd 1 = ) ;

\ Compute b/a (mod n)
: divide ( a b n -- x )
  3dup nip divide-assert
  rot ( b n a )
  over swap ( b n n a )
  dio-dxy ( b n d t s )
  nip nip ( b n s )
  dup 0 < if over + then
  rot * swap mod ;

\ minimum divisor
: md ( n - d )
  dup 1+ 2 do
    dup i mod 0= if drop i leave then
    dup i i * <= if leave then
  loop ;

\ Chinese Remainder Theorem ( Sunzi )

: crt ( a ra b rb -- n )
  rot swap 2swap ( ra rb a b )
  2dup * >r ( ra rb a b -- R: ab )
  2dup egcd rot drop ( ra rb a b x y -- R: ab )
  >r rot * ( ra rb b ax -- R: ab y )
  rot * ( ra b rbax -- R: ab y )
  -rot r> * * ( rbax raby -- R: ab )
  + r> mod ;

  
