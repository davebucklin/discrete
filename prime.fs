\ Test for the Always Prime? puzzle in the Mathematical Thinking in 
\ Computer Science course. Find a counter-example to the assertion:
\ for every integer n>1 the number n^2+n+41 is prime
\ Uses the 6k+-1 test for prime numbers.

\ n squared + n + 41
: compute ( n -- n )
  dup dup * + 41 + ;

: 2or3? ( n -- f )
  dup 2 = swap 3 = or ;

: /2or3? ( n -- f )
  dup 2 /mod drop 0 <> swap 3 /mod drop 0 <> and invert ;

: /6k~1? ( n -- f )
  5 ( n 5 )
  begin
    ( n 5 ) 2dup /mod drop 0 <> -rot
    ( f n 5 ) 2dup 2 + /mod drop 0 <> -rot
    ( f f n 5 ) 2swap
    ( n 5 f f ) and invert if 2drop 0 exit then
    2dup ( n 5 n 5 )
    dup * swap <= while
    ( n 5 ) 6 + 
  repeat 2drop -1 ;

: prime? ( n -- f )
  dup 2or3? if drop -1 exit then
  dup /2or3? if drop 0 exit then
  /6k~1? ;

: main ( -- )
  100 ( 1000000 ) 2 do
    i compute prime? invert if cr i . i compute . then
  loop ;
