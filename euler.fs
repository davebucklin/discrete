\ Find the shortest cycle that visits every edge in an Eulerian graph
\ ( all vertices have an even degree ) My plan is to apply Fleury's
\ algorithm to find, hopefully, the shortest Eulerian cycle in a graph

variable location 0 location !  \ current vertex
variable edges 0 edges !          \ pointer into edges linked list

\ linked list routines for storing the list of edges item structure
\ [next][head][tail][visited] Almost certainly an abuse of this data
\ structure given the links are not really meaningful

\ create an edge
: edge, ( head tail -- )
  here -rot edges @ , , , 0 , edges !  ;

: data> ( edge -- head tail )
  cell + 2@ ;

: next> ( edge -- edge' )
  @ ;

: 'visited ( edge -- addr )
  3 cells + ;

: visited> ( edge -- f )
  'visited @ ;

\ print out the list of edges
: .edges ( -- )
  cr edges @ begin dup while dup data> . . dup visited> . ." ->" next> repeat cr drop ;

: e, edge, ; ( head tail -- )

\ I think we're going to be real dumb here and just return the first
\ edge that's not visited
: nextedge ( vertex -- edge )
  >r edges @
  begin 
    dup 0 <> if ( edges )
      dup data> ( edges head tail )
      r@ = swap r@ = or ( edges f ) \ is this edge incident to the vertex?
      if dup visited> else -1 then  \ if the edge has not been visited, we can stop looking
    else 0 ( edges f ) then
  while next>
  repeat
  rdrop ;

\ follow this edge. Update our location and mark as visited.
: follow ( edge -- )
  dup data> dup location @ = if drop else nip then location !
  -1 swap 'visited ! ;

\ clear visited flag on all edges
: unvisit ( -- )
  edges @ begin dup while dup 'visited 0 swap ! next> repeat drop ;

: reset ( -- )
  0 location !
  unvisit ;

\ start at current location, follow edges until we run out.
: walk ( -- )
  reset 0
  begin
    location @      \ get the current location
    dup . ." -> "    
    nextedge        \ get the next edge
  dup while
    follow          \ follow the edge
    1+              \ track path length
  repeat 
  drop cr ." length=" . ;

: unvisited? ( -- f )
  0 edges @
  begin
  dup while
    dup visited> invert rot or swap next> \ or !visited flag with initial flag
  repeat
  drop ;

\ For now, this will have to be a tag-team between me and the computer.
\ If it fails to visit all nodes, I will alter the node order and let it
\ try again.  Eventually, I want to implement the third rule in Fleury's
\ algorithm and detect whether removing the next edge disconnects
\ a component.  If we get to the end and all nodes are not visited,
\ back up until there's a fork.  Move first edge in the fork to the end
\ of the list and try again.

: makeedges ( -- )
  \ rows
  0 1 e, 1 2 e, 2 3 e, 3 4 e,
  5 6 e, 6 7 e, 7 8 e, 8 9 e, 
  10 11 e, 11 12 e, 12 13 e, 13 14 e, 
  15 16 e, 16 17 e, 17 18 e, 18 19 e, 
  20 21 e, 21 22 e, 22 23 e, 23 24 e,
  \ columns
  0 5 e, 5 10 e, 10 15 e, 15 20 e,
  1 6 e, 6 11 e, 11 16 e, 16 21 e,
  2 7 e, 7 12 e, 12 17 e, 17 22 e,
  3 8 e, 8 13 e, 13 18 e, 18 23 e,
  4 9 e, 9 14 e, 14 19 e, 19 24 e,
  \ make it Eulerian
  23 22 e, 21 20 e, 20 15 e, 10 5 e, 1 2 e, 3 4 e, 4 9 e, 14 19 e, ;
