\ Fast modular exponentiation

\ b ** 2 ** k mod m
: fme ( b k m -- n )
  -rot >r over mod ( m b%m -- r:k )
  r> 0 do dup * over mod ( m b%m' ) loop nip ;

: ^ ( a b -- a**b )
  over swap 1 ?do over * loop nip ;

\ is 2^n a factor of e?
: 2^n? ( n e -- f )
  1 rot lshift and ;

\ could also try, more explicitly, the square/multiply method from Numberphile

\ is b^idx <= e?
: continue? ( e b idx -- f )
  ^ <= ;

: 3dup ( a b c -- a b c a b c )
  2 pick 2 pick 2 pick ;

\ b ** e mod m
: fme2 ( b e m -- n )
  \ mod b and set up the stack
  rot ( e m b )
  over ( e m b m )
  mod ( e m b%m )
  swap >r ( e b%m -- r:m )
  dup >r ( e b%m -- r:m b%m )
  \ get starting b
  over 1 ( e b e 1 )
  and invert if drop 1 then ( e b )
  1 begin ( e b idx )
  3dup ( e b idx e b idx )
  continue? while ( e b idx )
    rot ( b idx e )
    \ if e contains the idx'th power of two,    
    2dup 2^n? if 
      \ set up fme stack ( b k m )
      ( b idx e )
      swap over ( b idx e idx )
      2r@ ( b idx e idx m b%m )
      -rot ( b idx e b%m idx m )
      fme ( b idx e b' )
      -rot ( b b' idx e )
      2swap * ( idx e b' )
      2r@ drop mod
      -rot ( b' idx e )
    then
    swap 1+ swap
  repeat
  \ cleanup stack
  drop drop rdrop rdrop ;

\ completes, but giving wrong answers

: check ( a b -- )
  cr = if ." PASS" else ." FAIL" then cr ;

: test
  2 238 239 fme2 1 check
  2 953 239 fme2 2 check
  34 60  77 fme2 1 check ;

