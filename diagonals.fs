\ For the Mathematical Thinking in Computer Science course.
\ Find 16 diagonals on a 5x5 grid that don't touch.
\ This is intended to demonstrate backtracking.
\ I'm not sure I've done that, but it's also not a brute-force approach.

25 constant size
create grid size cells allot

variable index          \ current pointer into the grid
variable diagonals      \ current count of diagonals
variable attempts       \ count the number of permutations

: init ( -- )
  grid size cells erase
  0 index !
  0 diagonals ! ;

: increment ( addr -- )
  1 swap +! ;

: decrement ( addr -- )
  dup @ 1- swap ! ;

: box ( -- addr )
  index @ cells grid + ;

: ?diagonal ( value -- )
  dup 1 = if diagonals increment then
  3 = if diagonals decrement then ;

: toobig? ( value -- f )
  3 > ;

\ When in doubt, use brute force. - Ken Thompson - Michael Scott

\ conflict tests

: uu ( f -- f )
  dup if exit then
  index @ 5 + size < if
    box @
    grid index @ 5 + cells + @
    + 3 = if drop -1 then
  then ;

: ur ( f -- f )
  dup if exit then
  index @ 6 +
  dup size < swap 5 /mod drop 0 <> and if
    box @
    grid index @ 6 + cells + @
    1 = swap 1 = and if drop -1 then
  then ;

: rr ( f -- f )
  dup if exit then
  index @ 1+
  dup size < swap 5 /mod drop 0 <> and if
    box @
    grid index @ 1+ cells + @
    + 3 = if drop -1 then
  then ;

: dr ( f -- f )
  dup if exit then
  index @ 4 -
  dup 0 >= swap 5 /mod drop 0 <> and if
    box @
    grid index @ 4 - cells + @
    2 = swap 2 = and if drop -1 then
  then ;

: dd ( f -- f )
  dup if exit then
  index @ 5 -
  0 >= if
    box @
    grid index @ 5 - cells + @
    + 3 = if drop -1 then
  then ;

: dl ( f -- f )
  dup if exit then
  index @ 6 -
  dup dup 0 > swap 1+ 5 /mod drop 0 <> and swap 0= or if
    box @
    grid index @ 6 - cells + @
    1 = swap 1 = and if drop -1 then
  then ;

: ll ( f -- f )
  dup if exit then
  index @ 1-
  dup 0 >= swap 1+ 5 /mod drop 0 <> and if
    box @
    grid index @ 1- cells + @
    + 3 = if drop -1 then
  then ;

: ul ( f -- f )
  dup if exit then
  index @ 4 +
  dup size < swap 1+ 5 /mod drop 0 <> and if
    box @
    grid index @ 4 + cells + @
    2 = swap 2 = and if drop -1 then
  then ;

: conflict? ( -- f )
  0 uu ur rr dr dd dl ll ul ;

: beyond? ( -- f )
  index @ size >= ;

create chars char ? , char / , $5c , char _ , char X , char Y ,

: .box ( addr -- )
  bl emit @ cells chars + @ emit bl emit ;

: .grid ( -- )
  5 0 do
    cr 4 i - 5 * dup 5 + swap do
      grid i cells + .box
    loop
  loop cr ;

: done? ( -- )
  index @ 0 < ;

\ increment the cell
\ either
\   1. cell value is too big - reset and decrement index, repeat
\   2. cell has conflict - repeat w/o advance
\   3. cell is ok - advance pointer, done

: check? ( value -- f )
  dup 1 = swap 2 = or ;

: main ( -- )
  init
  begin
    box @ 1+ dup ?diagonal
    dup toobig? if
      drop
      0 box !
      index decrement
    else
      dup box !
      check? if conflict? invert else -1 then if
        index increment
        beyond? if
          attempts increment
          diagonals @ dup
          16 >= if
            cr . ." Solution!" cr
            .grid
          else
            drop \ cr . ." :(" cr
          then
          index decrement
        then
      then
    then
  done? until 
  cr ." Attempts: " attempts @ . cr ;

