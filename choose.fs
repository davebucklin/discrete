\ choose function for the Combinatorics and Probability course
\ Gives incorrect answers if intermediate values (e.g., n!) exceed 2^64

: fact ( n -- n! )
  1 swap 1+ 2 do i * loop ;

: choose ( n k -- u )
  2dup - fact swap fact * swap fact swap / ;

