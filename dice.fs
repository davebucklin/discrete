\ Combinatorics & Probaility Final Project: Dice Game

create dice 4 cells allot       \ dice pointer array
create wins 4 cells allot       \ wins array
create faces 4 cells allot      \ face index for each die
create buffer 32 cells allot    \ string input buffer
variable #dice                  \ number of dice entered
6 constant #faces

\ dice comparison variables
variable twoequal 0 twoequal !  \ two or more dice are equal flag
variable highval 0 highval !    \ current highest face value
variable highdie 0 highdie !    \ position of higest face value
create winmatrix 16 cells allot

: set ( addr -- )
  -1 swap ! ;

: clear ( addr -- )
  0 swap ! ;

\ zero everything out
: reset ( -- )
  dice 4 cells erase
  wins 4 cells erase
  faces 4 cells erase
  winmatrix 16 cells erase
  #dice clear ;

\ get a string at buffer
: get ( -- len )
  buffer 254 accept ;

\ add a die to list of dice, increase #dice
: new ( -- )
  here dice #dice @ cells + !
  1 #dice +! ;

\ convert comma-delimited string at buffer into numbers and store dice
\ make sure all your dice have six sides!
: build ( len -- )
  #dice clear
  ( len ) 0 do
    buffer i + c@
    dup [char] [ = if new then
    dup dup 47 > swap 58 < and if dup 48 - , then \ convert ascii to value
    drop
  loop ;

\ get pointer to die n, zero origin
: >die ( n -- addr )
  dice swap cells + @ ;

\ get pointer to current face for a given die
: >face ( die -- addr )
  dup >die swap cells faces + @ cells + ;

\ dump dice
: .dice
  #dice @ 0 do cr ." [ " #faces 0 do j >die i cells + @ . ." ," loop ." ]," loop ;

\ Find the highest die

\ The die in slot 0 is compared against all the other dice.
\ For each permutation of faces, wins are tallied for the higest face value.

: .wins ( -- )
  cr ." (" #dice @ 0 do wins i cells + @ . ." ," loop ." )" ;

: setup ( -- )
  0 highdie ! 0 >face @ highval ! twoequal clear ;

: ?equal ( n n -- )
  = if twoequal set then ;

: biggest ( val die -- )
  highdie ! highval ! twoequal clear ;

\ rotate dice faces
: flip ( -- carry )
  -1 #dice @ 0 do \ for each die
    dup if drop faces i cells + @ \ if carry, get face index value
      dup 5 = if
        drop 0 faces i cells + ! -1 \ if face = 5, set to 0 and set carry
      else
        1+ faces i cells + ! 0
      then
    then
  loop ;

: .roll ( -- )
  #dice @ 0 do cr ." die " i . ." face " faces i cells + @ . ." value " i >face @ . loop ;

\ if !equal flag, increment wins for highdie
: ?winner ( -- )
  twoequal @ invert if highdie @ wins highdie @ cells + 1 swap +! then ;

: compare ( -- )
  faces 4 cells erase   \ set all faces to zero
  begin
    setup               \ set default highval and highdie from die 0
    #dice @ 1 do        \ for each remaning die, compare current face to high value
      i >face @ highval @
      2dup ?equal       \ if equal, set equal flag
      over < if i biggest else drop then \ if bigger, set high value, high die, clear equal flag
    loop
  ?winner flip until ;  \ increment faces until done

\ put a string where build can get to it
: import ( addr len -- )
  2dup cr type tuck buffer swap cmove reset build ;

: rollem ( addr len -- )
  import compare ;

\ First Task: Compare two dice
: first ( -- )
  cr ." Sample 1"
  s" [1, 2, 3, 4, 5, 6],[1, 2, 3, 4, 5, 6]" rollem .wins
  cr ." Sample 2"
  s" [1, 1, 6, 6, 8, 8],[2, 2, 4, 4, 9, 9]" rollem .wins ;

\ which die wins more than the others?
\ idx is -1 if there is no best die
: best? ( -- -1|idx )
  wins @ highval ! 0 highdie ! 0 twoequal ! \ set first die as default highest
  #dice @ 1 do          \ for each other die,
    wins i cells + @    \ get dice wins value
    highval @           \ get high value
    2dup ?equal         \ equality test
    over < if           \ bigger?
      highval ! i highdie ! twoequal clear
    else drop then
  loop
  twoequal @ if -1 else highdie @ then ;

\ Second Task: Is there a best die?
: second ( -- )
  cr cr ." =Sample 1="
  s" [1, 1, 6, 6, 8, 8], [2, 2, 4, 4, 9, 9], [3, 3, 5, 5, 7, 7]"
  rollem .wins cr ." Best=" best? .
  \ Output: -1
  cr cr ." =Sample 2="
  s" [1, 1, 2, 4, 5, 7], [1, 2, 2, 3, 4, 7], [1, 2, 3, 4, 5, 6]"
  rollem .wins cr ." Best=" best? .
  \ Output: 2
  cr cr ." =Sample 3="
  s" [3, 3, 3, 3, 3, 3], [6, 6, 2, 2, 2, 2], [4, 4, 4, 4, 0, 0], [5, 5, 5, 1, 1, 1]"
  rollem .wins cr ." Best=" best? .
  \ Output: -1
  ;

\ Third Task: Implement a Strategy

\ Having completed this task, I'm almost certain at least some of the logic
\ for previous tasks is incorrect. However, I'm not going to fix it because
\ there's no more sample data and it's just not worth the effort at this point.

variable choose-first       \ flag
variable first-dice         \ flag
create choice 4 cells allot \ for each die, the die chosen in response

\ get address in winmatrix for this pair
\ address will contain number of wins die a has over die b
: >winbox ( dieidxa dieidxb -- addr )
  swap 4 * + cells winmatrix + ;

: wins> ( die -- wins )
  cells wins + @ ;

: empty? ( addr -- f )
  0 = ;

defer fmt
: (rates) 100 * #faces #faces * / . ;
: (scores) . ;
: rates ['] (rates) is fmt ;
: scores ['] (scores) is fmt ;
scores

: .winmatrix ( -- )
  cr ."     "
  #dice @ 0 do i . bl emit bl emit loop
  #dice @ 0 do
    cr i . bl emit bl emit
    #dice @ 0 do
      j i <> if j i >winbox @ fmt else ." X  " then bl emit
    loop
  loop ;

create pair 2 cells allot
: pair>a pair ;
: pair>b pair cell + ;

: 2compare ( a b -- )
  pair>b ! pair>a !
  wins 4 cells erase
  #faces 0 do
    #faces 0 do
      pair>a @ >die j cells + @
      pair>b @ >die i cells + @ ( a b )
      2dup > if 1 wins pair>a @ cells + +! then
           < if 1 wins pair>b @ cells + +! then
    loop
  loop ;

: for-dice ( xt -- )
  #dice @ 0 do
    #dice @ 0 do
      j i <> if
        dup j i rot execute
      then
    loop
  loop
  drop ;

: stratagem ( j i -- )
  2dup >winbox @
  empty? if
    2dup 2compare
    2dup over wins> ( j i j i jwins )
    -rot >winbox ! ( j i )
    dup wins> ( j i iwins )
    -rot swap >winbox !
  else
    2drop
  then ;

\ compare each die against each other die
\ for a given die, is there another die that beats it?
: strategery ( -- )
  winmatrix 16 cells erase ['] stratagem for-dice ;

: tally-one ( a b -- )
  2dup >winbox @ ( j i jiwins )
  -rot over >winbox @ ( jiwins j ijwins )
  rot swap ( j jiwins ijwins )
  > if cells wins + 1 swap +! else drop then ;

\ add up per-die wins based on winmatrix data
: tally ( -- )
  wins 4 cells erase ['] tally-one for-dice ;

\ if one die wins over all the other dice, return its index, else -1
: best? ( -- -1|dieidx )
  tally -1 #dice @ 0 do i wins> #dice @ 1- = if drop i then loop ;

\ key-value max, if equal, ( key1 n1 ) is returned
: kvmax ( key1 n1 key2 n2 -- key1|key2 max[n1,n2] )
  rot 2dup > if drop rot drop else nip nip then ;

\ for each die, find the die that wins against it the most
: choices ( -- )
  choice 4 cells erase
  #dice @ 0 do
    \ get starting die,score for inner loop
    i 0 = if 1 else 0 then ( dieidx )
    i swap tuck >winbox @ ( dieidx score )
    #dice @ 0 do
      j i <> if
        j i tuck >winbox @ ( dieidx score dieidx2 score2 )
        kvmax
      then
    loop
    drop ( dieidx )
    choice i cells + !
  loop ;

\ opponent:mine
: .choices ( -- )
  #dice @ 0 do i . ." :" choice i cells + @ . ." ," loop ;

\ The assignment seems to want these key-value pairs flipped
\ relative to what I would expect.
\ mine:opponent
: .choicesbw ( -- )
  #dice @ 0 do choice i cells + @ . ." :" i . ." ," loop ;

\ decide what to do based on whether one die beats all the others
: strategize ( -- )
  strategery best?
  dup -1 = if
    drop choose-first clear choices
  else
    first-dice ! choose-first set
  then ;

: .strategy ( -- )
  cr ." {'choose_first':" choose-first @ if ." True," else ." False," then
  choose-first @ if
    ." 'first_dice':" first-dice @ .
  else
    .choicesbw
  then ." }" ;

: third ( -- )
  cr cr ." =Sample 1="
  s" [1, 1, 4, 6, 7, 8], [2, 2, 2, 6, 7, 7], [3, 3, 3, 5, 5, 8]"
  import strategize .strategy
  \ Output: {'choose_first': False, 0: 1, 1: 2, 2: 0}
  cr cr ." =Sample 2="
  s" [4, 4, 4, 4, 0, 0], [7, 7, 3, 3, 3, 3], [6, 6, 2, 2, 2, 2], [5, 5, 5, 1, 1, 1]"
  import strategize .strategy
  \ Output: {'choose_first': True, 'first_dice': 1}
  ;
